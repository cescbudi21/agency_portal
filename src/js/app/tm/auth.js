app.factory('tmAuth', ['Restangular', 'tmApi', '$localStorage', 'jwtHelper', function (Restangular, tmApi, $localStorage, jwtHelper) {

  return {
    userLogin: function(){
      return Restangular.service(tmApi.authToken);
    },
    userForgotPassword: function(){
      return Restangular.service(tmApi.passwordRecovery);
    },
    saveToken: function(token) {
      $localStorage.TM_TOKEN_STORAGE_KEY = token;
      $localStorage.TM_TOKEN_DECODED_KEY = this.decodeToken(token);
    },
    getToken: function() {
      return $localStorage.TM_TOKEN_STORAGE_KEY;
    },
    clearToken: function() {
      delete $localStorage.TM_TOKEN_STORAGE_KEY;
    },
    verifyToken: function() {
      return Restangular.one('auth').verify({token: this.getToken()});
    },
    decodeToken: function() {
      return jwtHelper.decodeToken(this.getToken());
    },
    setDefaultHeader: function() {
      Restangular.setDefaultHeaders({token: self.getToken()});
    }
  };
}]);
