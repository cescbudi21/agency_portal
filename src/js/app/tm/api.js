app.factory('tmApi', function () {
  return {
    authToken: 'auth/token/',
    verifyToken: 'auth/verify',
    passwordRecovery: 'pass/recovery/',
    passwordReset: 'pass/reset/',
  };
});
