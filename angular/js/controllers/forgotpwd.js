'use strict';

/* Controllers */
// signin controller
app.controller('ForgotPwdFormController', ['$scope', '$state', 'tmAuth', function($scope, $state, tmAuth) {
  $scope.user = {
    username: '',
    email: ''
  };
  $scope.authError = null;

  $scope.doSubmit = function() {
    tmAuth.userForgotPassword().post($scope.user).then(function(data) {
      $state.go('access.signin')
    },function(err){
      if (err.status == 500)
      {
        $scope.authError = {
          non_field_errors: [err.status + ", " + err.statusText]
        };
      }
      else
        $scope.authError = err.data;
    });
  }


}])
;
