'use strict';

/* Controllers */
  // signin controller
app.controller('SigninFormController', ['$scope', '$http', '$state', 'tmAuth', function($scope, $http, $state, tmAuth) {
    $scope.user = {
        username: '',
        password: ''
    };
    $scope.authError = null;

    $scope.doLogin = function() {
      tmAuth.userLogin().post($scope.user).then(function(data) {
        var token = data.token;

        // Store Token to storage
        tmAuth.saveToken(token);
        // Redirect state to dashboard
        $state.go('app.dashboard');

      },function(err){
        if (err.status == 500)
        {
          $scope.authError = {
            non_field_errors: [err.status + ", " + err.statusText]
          };
        }
        else
          $scope.authError = err.data;
      });
    }
  }])
;
