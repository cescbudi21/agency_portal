// Configurations file

// URL for API defined here..
var BASE_URL_API = 'api-agent.tokiomarine-life.co.id/'

// Set base URL for REST, use local cors-proxy if domain == ap.dev
// https://github.com/gr2m/CORS-Proxy

// TODO: Should be removed when in production
var CORS_PROXY = '';
if (window.location.hostname == 'localhost' || window.location.hostname == 'ap.dev') {
  CORS_PROXY = 'localhost:1234/';
}

var URL_API = 'http://' + CORS_PROXY + BASE_URL_API;
