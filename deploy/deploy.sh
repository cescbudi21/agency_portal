#!/bin/sh

#
# software deployment script -- run via jenkins or from the command line
#
# usage: deploy.sh server.name
#

SCRIPT=`readlink -f $0`
DIRNAME=`dirname $SCRIPT`
STARTDIR=`dirname $DIRNAME`

##################################################################################
# Get config
##################################################################################

if [ $# -ne 1 ]; then
    echo "usage: $0 servername"
    exit 1
fi
TARGET=$1

cd $STARTDIR
if [ ! -f deploy/$TARGET.conf ]; then
    echo "No config file available: deploy/${TARGET}.conf"
    exit 1
fi

cd $STARTDIR
. deploy/${TARGET}.conf
echo "Deploying to ${TARGETUSER}@${TARGETSERVER}:${TARGETDIR}"

. deploy/sources.conf
echo "Deploying ${SOURCES}"

##################################################################################
# Build server infrastructure directories
##################################################################################

#
# Check user account
#
cd $STARTDIR
echo "Checking ${TARGETUSER} account and creating if required"
ssh root@${TARGETSERVER} "
    if [ ! -d /home/${TARGETUSER} ]; then
        adduser -c 'Web site owner' ${TARGETUSER} -G ${WEBGROUP}
        mkdir /home/${TARGETUSER}/.ssh
        cp /root/.ssh/id_dsa /home/${TARGETUSER}/.ssh/
        cp /root/.ssh/id_dsa.pub /home/${TARGETUSER}/.ssh/
        cp /root/.ssh/authorized_keys /home/${TARGETUSER}/.ssh/
        chown -R ${TARGETUSER}:${TARGETUSER} /home/${TARGETUSER}
    fi
"

#
# Set up deployment directories
#
echo "Setting up target directories $TARGETDIR"
ssh root@${TARGETSERVER} "
    mkdir -p $TARGETDIR
    chown -R ${TARGETUSER}:${TARGETUSER} $TARGETDIR
"

##################################################################################
# Deploy web site software
##################################################################################

#
# Make a backup before starting
#
if [ "$TARGETDEPLOYDB" != "new" ]; then
    echo "Backing up existing web files and database on $TARGETSERVER"
    BACKDATE=`date '+%Y%m%d_%H%M'`
    DIR=`basename $TARGETDIR`
    BACKDIR=${BACKDATE}_${DIR}
    ssh root@${TARGETSERVER} "
        cd $TARGETDIR
        cd ..
        mkdir -p backups
        cp -a $TARGETDIR backups/$BACKDIR
        if [ "$DBBACKUP" = "yes" ]; then
            mysqldump -u $TARGETDBUSER --password=$TARGETDBPASS -h ${TARGETDBHOST} ${TARGETDBNAME} | gzip > backups/${BACKDATE}_${TARGETDBNAME}.sql.gz
        else
            echo "Database backup disabled on $TARGETSERVER. Change DBBACKUP=yes in your config file to enable"
        fi
    "
fi

#
# Deploy web software files
#
cd $STARTDIR
echo "Syncing web files to $TARGETDIR"
rsync -vazR --delete --exclude=*.pyc $SOURCES ${TARGETUSER}@${TARGETSERVER}:${TARGETDIR}

#
# Deploy modified config files
#
#echo "Deploying config files in ${STARTDIR}/deploy/${TARGETSERVER}"
#cd ${STARTDIR}/deploy/${TARGETSERVER}
#for file in *; do
#    TARGETFILE=`echo $file`
#    echo "Copying $file to ${TARGETSERVER}:${TARGETDIR}/${APPNAME}/${TARGETFILE}"
#    scp $file ${TARGETUSER}@${TARGETSERVER}:${TARGETDIR}/${APPNAME}/${TARGETFILE}
#done

#
# Install root config files.
#
echo "Installing root configuration files on ${TARGETSERVER}"
cd ${STARTDIR}/deploy/root/${TARGETSERVER}
for file in *; do
    TARGETFILE=`echo $file | sed 's;+;/;g'`
    echo "Copying $file to ${TARGETSERVER}:/${TARGETFILE}"
    scp $file root@${TARGETSERVER}:/${TARGETFILE}
done

#
# Web user config post install
#
ssh ${TARGETUSER}@${TARGETSERVER} "
    cd $TARGETDIR
    if [ -f bower.json ]; then
        echo "Running bower install"
        bower install --quiet
    fi
    echo "Running npm install"
    npm install --quiet
"

#
# Create symlink to web public directory
#
ssh ${TARGETUSER}@${TARGETSERVER} "
    if [ ! -d $TARGETDIR/public ]; then
        echo "Create symlink from app to public directory"
        ln -s ${TARGETDIR}/src ${TARGETDIR}/public
    fi
"

#
# Reset permissions
#
echo "Fixing permissions"
ssh root@${TARGETSERVER} "
    chown -R ${TARGETUSER}:${WEBGROUP} ${TARGETDIR}
    chmod -R 775 ${TARGETDIR}
    find ${TARGETDIR} -type d -exec chmod 2775 {} \;
"

#
# Reload nginx and uwsgi
#
echo "Reloading nginx"
ssh root@${TARGETSERVER} "
    service nginx reload > /dev/null 2>&1
"
