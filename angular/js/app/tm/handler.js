app.factory('tmHandler', ['$q', '$http', '$timeout', 'tmAuth',
  function($q, $http, $timeout, tmAuth) {
    return {
      isNonFieldError: function(err) {
        if (err.non_field_errors != undefined) {
          return true;
        }
        return false;
      },
      getNonFieldError: function(err) {
        if (this.isNonFieldError(err)) {
          return err.non_field_errors;
        }
        return [];
      },
      handleTokenExpired: function(err) {
        var errMsg = this.getNonFieldError(err);
        if (errMsg[0] == TM_ERR_TOKEN_EXPIRED) {
          tmAuth.clearToken();
        }
      }
    }
  }]);
