## Tokio Marine Life - Agency Portal ##


### Pre-requisites : ##
* [NodeJS](https://nodejs.org/en/download/)
* Bower : `npm install -g bower`
* Yeoman : `npm install -g yo`
* Compass : `gem install compass`
* Cors-Proxy : `npm install -g cors-proxy` (CORS issue for local development)

### Installing components ###
* `cd <into_directory_of_ap_web_client>`
* `bower install`
* `npm install`

### Running local server : ###
* `grunt server`
