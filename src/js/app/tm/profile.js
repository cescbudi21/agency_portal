app.factory('tmProfile', ['$localStorage', function ($localStorage) {

  return {
    getProfile: function() {
      return $localStorage.TM_TOKEN_DECODED_KEY;
    },
    getProfileName: function() {
      var profile = this.getProfile();
      return (profile != null) ? profile.username : "";
    }
  }
}]);
