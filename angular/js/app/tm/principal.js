app.factory('tmPrincipal', ['$q', '$http', '$timeout', 'tmAuth', 'tmHandler',
  function($q, $http, $timeout, tmAuth, tmHandler) {
    var _identity = undefined,
      _authenticated = false;

    return {
      isIdentityResolved: function() {
        return angular.isDefined(_identity);
      },
      isAuthenticated: function() {
        return _authenticated;
      },
      isInRole: function(role) {
        if (!_authenticated || !_identity.roles) return false;

        return _identity.roles.indexOf(role) != -1;
      },
      isInAnyRole: function(roles) {
        if (!_authenticated || !_identity.roles) return false;

        for (var i = 0; i < roles.length; i++) {
          if (this.isInRole(roles[i])) return true;
        }

        return false;
      },
      authenticate: function(identity) {
        _identity = identity;
        _authenticated = identity != null;
      },
      identity: function(force) {
        var deferred = $q.defer();

        if (force === true) _identity = undefined;

        tmAuth.verifyToken().then(function(data){
          _identity = tmAuth.decodeToken(data.token);
          _authenticated = true;
          deferred.resolve(_identity);
        },function(err){
          tmHandler.handleTokenExpired(err.data);
          _identity = null;
          _authenticated = false;
          deferred.resolve(_identity);
        });

        return deferred.promise;
      }
    };
  }
])
.factory('authorization', ['$rootScope', '$state', 'tmPrincipal', '$location', '$q',
  function($rootScope, $state, principal, $location, $q) {
    return {
      authorize: function() {
        return principal.identity()
          .then(function() {
            var isAuthenticated = principal.isAuthenticated();

            if ($rootScope.toState.data) {
              if ($rootScope.toState.data.roles && $rootScope.toState.data.roles.length > 0 && !principal.isInAnyRole($rootScope.toState.data.roles)) {
                if (isAuthenticated) $state.go('accessdenied'); // user is signed in but not authorized for desired state
                else {
                  // user is not authenticated. stow the state they wanted before you
                  // send them to the signin state, so you can return them when you're done
                  $rootScope.returnToState = $rootScope.toState;
                  $rootScope.returnToStateParams = $rootScope.toStateParams;

                  // now, send them to the signin state so they can log in

                  $location.path('/access/signin');
                }
              }
            }
            else {
              if (!isAuthenticated) {
                $rootScope.returnToState = $rootScope.toState;
                $rootScope.returnToStateParams = $rootScope.toStateParams;

                // now, send them to the signin state so they can log in
                //$state.go('access.signin');
                $location.path('/access/signin');
              }
            }
          });
      }
    };
  }
])
